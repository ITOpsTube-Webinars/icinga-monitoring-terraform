resource "aws_instance" "icinga" {
  ami = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.medium"
  vpc_security_group_ids = [ "${aws_security_group.icinga_sg.id}" ]
  key_name = "${aws_key_pair.icingakey.key_name}"

   provisioner "local-exec" {
     command = "sleep 120 && echo \"[monitoring_servers]\n${aws_instance.icinga.public_ip} ansible_connection=ssh ansible_ssh_user=centos ansible_ssh_private_key_file=icingakey host_key_checking=False\" > icinga-inventory &&  ansible-playbook -i icinga-inventory ansible-playbooks/site-icinga.yml"
  }

  connection {
    user = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
}


output "icinga_ip" {
    value = "${aws_instance.icinga.public_ip}"
}
output "Icinga_Setup_URL" {
    value = "http://${aws_instance.icinga.public_ip}/icingaweb2/setup"
}


