variable "AWS_REGION" {
  default = "us-east-1"
}
variable "AMIS" {
  type = "map"
  default = {
    us-east-1 = "ami-b81dbfc5"
    us-west-2 = "ami-223f945a"
    eu-west-1 = "ami-844e0bf7"
  }
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "icingakey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "icingakey.pub"
}
variable "INSTANCE_USERNAME" {
  default = "centos"
}
variable "icingavpc" {
  default = "vpc-52cdc535"
}

