resource "aws_key_pair" "icingakey" {
  key_name = "icingakey"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
}

